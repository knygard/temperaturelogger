import glob
import time

devicelist = glob.glob('/sys/bus/w1/devices/28*')
devicefile = devicelist[0] + '/w1_slave'

fileobj = open(devicefile, 'r')
lines = fileobj.readlines()
fileobj.close()

lines[0].strip()
lines[1].strip()

def read_line():
    """ Return raw strings from device """
    return lines[0] + '\n' + lines[1]

def read_temperature():
    """ Return temperature as float in celsius """
    return float(int(lines[1][-6:]))/1000

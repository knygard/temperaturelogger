# TemperatureLogger

A temperaturelogging system utilizing Raspberry Pi, DS18B20 sensor and SQLAlchemy.

__Development photo:__

![Developing the logger](http://bytebucket.org/knygard/temperaturelogger/raw/d6e2b423edca02c599ac320066f7f71c18f3b3dc/photos/development.jpg)
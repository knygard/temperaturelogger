from settings import *
from ds18b20 import read_temperature
from Models import Temperature, Stats
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

engine = create_engine('sqlite:///' + '/var/db/' + DB_NAME)

# Create Tables
Temperature.metadata.create_all(engine)
Temperature.metadata.bind = engine
Stats.metadata.create_all(engine)
Stats.metadata.bind = engine

# Create session and add temperature
DBSession = sessionmaker(bind=engine)
session = DBSession()
temperature = read_temperature()
session.add(Temperature(sensorid=1, temperature=temperature))
session.merge(Stats(key="latest_temperature", value=temperature))

# Commit session
session.commit()

# Send to heroku backend
import json, urllib2, base64
data = json.dumps({'temperature': temperature, 'sensorid': 1})
clen = len(data)
username = 'SET_USERNAME_HERE'
password = 'SET_PASSWORD_HERE'
url = 'https://SET_URL_HERE'
request = urllib2.Request(url, data, {'Content-Type': 'application/json', 'Content-Length': clen})
base64string = base64.encodestring('%s:%s' % (username, password)).replace('\n', '')
request.add_header("Authorization", "Basic %s" % base64string)
f = urllib2.urlopen(request)
response = f.read()
print response
f.close()

import datetime
from sqlalchemy import Column, Integer, Float, DateTime, String
from sqlalchemy.ext.declarative import declarative_base

Base = declarative_base()

class Temperature(Base):
    __tablename__ = 'temperatures'
    id = Column(Integer, primary_key=True)
    sensorid = Column(Integer)
    temperature = Column(Float(precision=3))
    timestamp = Column(DateTime, default=datetime.datetime.now)

class Stats(Base):
    __tablename__ = 'stats'
    key = Column(String, primary_key=True)
    value = Column(Float(precision=3))
